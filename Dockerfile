FROM gcr.io/google_appengine/python

# Create a virtualenv for dependencies. This isolates these packages from
# system-level packages.
RUN virtualenv -p python3.6 /env


ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH

ADD gRPC_route_guide /opt/gRPC_route_guide

WORKDIR /opt/gRPC_route_guide/
RUN pip install -r requirements.txt

EXPOSE 50051

ENTRYPOINT ["python", "/opt/gRPC_route_guide/route_guide_server.py"]




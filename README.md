## Clone the repo 

	git clone https://gitlab.com/Syd.Quantiphi/grpc-k8s.git 

## Install python modules 
	
	pip install grpcio-tools
	pip install googleapis-common-protos 

## Run gRPC client App

	python grpc-k8s/gRPC_route_guide/route_guide_client.py 

